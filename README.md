# xlsx_to_image

### main.py will convert test.xlsx to image base64 string (result.txt)

$ docker build -t xlsx2image .
$ docker run -it --rm -v $(pwd)/:/usr/src/app/ xlsx2image
