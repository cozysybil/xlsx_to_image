FROM ubuntu

WORKDIR /usr/src/app

COPY . .

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y
RUN apt-get install -y python3 python3-pip
RUN pip3 install -r requirements.txt
RUN apt-get install -y wkhtmltopdf

CMD ["main.py"]
ENTRYPOINT ["python3"]