from xlsx2html import xlsx2html
import imgkit
from PIL import Image
import base64
from io import BytesIO

# xlsx -> html
out_steam = xlsx2html('test.xlsx')
str_html = out_steam.getvalue()

# html -> image
img = imgkit.from_string(str_html, False)

# image to base64
image = Image.open(BytesIO(img))
buffered = BytesIO()
image.save(buffered, format="PNG")
base64_byte = base64.b64encode(buffered.getvalue())
base64_str = base64_byte.decode("utf-8") 

# write result to txt
f = open("result.txt", "w")
f.write(base64_str)
f.close()